<?php get_header(); ?>

<div class="container pt-5 pb-5">
	
	<h1><?php the_title(); ?></h1>
	<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
		<?php the_content(); ?>
	<?php endwhile; endif; ?>
	
	
	<?php 
	//the_field('page_new_title'); 
	$title = get_field('page_new_title');
	$description = get_field('description');
	$age = get_field('age');
	echo 'Title : <br>' . $title . '<br><br>';
	echo 'Notes : <br><pre>' . $description . '</pre><br><br>';
	echo 'Age : <br>' . $age . '<br><br>';
	//the_field('image'); 
	?>
	<img src="<?php the_field ('image'); ?>" width="800" />
	<?php
	echo '<br><br>';
	//the_field('file'); 
	?>
	<a href="<?php the_field ('file'); ?>" target="_blank">Download PDF</a>
	<?php
	echo '<br><br>';
	the_field('datetime'); 
	echo '<br><br><br>';
	?>
	
	
	
	<?php
		echo 'Get all "Vehicle Custom Post Type" posts. <br><br>';
		$vehicles = new WP_Query(array(
			//'posts_per_page' => 2,
			'post_type' => 'vehicle'
		));
		
		while($vehicles->have_posts()) {
			$vehicles->the_post(); 
	?>
		
		<div><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
	
	<?php
		}
	?>
	
	
	
	
</div>




<?php get_footer(); ?>
