<?php
/**
 * Plugin Name: Plugin One
 * Plugin URL: https://synepco.com/CRH/
 * Description: My test plugin one
 * Version: 0.1
 * Author: Lahiru
 * Author URL: https://synepco.com/
 **/

//Add header bar
add_action('wp_body_open', 'addHeader');

function get_user_or_websitename(){
    if(!is_user_logged_in()){
        return 'to ' . get_bloginfo('name');
    }else{
        $current_user = wp_get_current_user();
        return $current_user -> user_login;
    }
}

function addHeader(){
    //echo '<h3 class="tb">Welcome to Test Plugin</h3>';
    echo <<<'EOD'
        <div style="padding: 20px; background: green;">
            <h2 class="tb">Welcome         
    EOD;   

    echo get_user_or_websitename();
     
    echo <<<'EOD'
            </h2>
        </div>
    EOD;
}

//Add CSS to the top bar
add_action('wp_print_styles', 'addCss');

function addCss(){
    echo '
        <style>
            h2.tb {
                color: white;
                text-align: center;
            }
        </style>
    ';
}
?>