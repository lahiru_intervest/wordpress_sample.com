<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress_db' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '123' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/** Add manually */
define('FS_METHOD', 'direct');



/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '+/`jOV7M(C/l>eCp)<_1s,8aGVXy9h>8h^p.Q/6lBwhDx+Fxr<#tlKT+PV8#5m$-' );
define( 'SECURE_AUTH_KEY',  '0?gm9F9+1`[Paf~P.KC%(;vXu1R&%GAI0Gy!&}>KSu$dBxCkaY2`J~q~JmlgTIqg' );
define( 'LOGGED_IN_KEY',    '2&JBJ:@ecys^2(4NLkF>:{54W)rd#iN#S8[eboR ,P@m0_O{KS)P 2&Z1&.z,O+L' );
define( 'NONCE_KEY',        '],apT?lUh>8GOJ^Zn_Sc%R8M:;[-71o rf]:)EPnk7qnM#53_UWYdx o2FI^RWEq' );
define( 'AUTH_SALT',        ',RnUq 6;txw(;Q_S1,H`{=[^SyferjI6YLN{4kh~ce8E^q~AU^U-dG6WK9>b_9`%' );
define( 'SECURE_AUTH_SALT', '@.PM)-i**=>>xf4(:m5{T2We^ZGK9TaXy&=YfEUclt,@u@z]22|HML%!K$qCT/^$' );
define( 'LOGGED_IN_SALT',   '~&+q aRSCMe6i)|=om,!ix8XPWmQ?w8?MC74lflo8DZ.Q!X$bg!YDmSmL)?<Tc:3' );
define( 'NONCE_SALT',       '}w=P8ewtqe&=7PX AteY>^$DMVvMvEOQHT#s:#6Zdsk!&&;iXJ$}[h)BP NJ8a,T' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
